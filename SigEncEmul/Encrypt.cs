﻿using System;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Text;

namespace SigEncEmul
{
    public class Encrypt
    {
        static byte[] resp = new byte[0];

        /// <summary>
        /// Метод шифрования и подписи для НБКИ
        /// </summary>
        /// <param name="clearRequest"></param>
        /// <param name="fileResultPath"></param>
        /// <param name="CertThumbprint"></param>
        public static void Nbch(byte[] clearRequest, string fileResultPath, X509Certificate2 CertThumbprint)
        {
            // Подпись
            try
            {
                resp = Cryptography.CryptoPro.SignMsg(clearRequest, CertThumbprint);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            // Шифрование
            try
            {
                resp = Cryptography.CryptoPro.EncryptMsg(resp, new X509Certificate2Collection() { CertThumbprint });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            File.WriteAllBytes(fileResultPath, resp);
        }

        /// <summary>
        /// Метод шифрования и подписи для ОКБ
        /// </summary>
        /// <param name="clearRequest"></param>
        /// <param name="fileResultPath"></param>
        /// <param name="CertThumbprint"></param>
        public static void Experian(byte[] clearRequest, string fileResultPath, X509Certificate2 CertThumbprint)
        {
            string respString;
            // Подпись
            try
            {
                resp = Cryptography.CryptoPro.SignMsg(clearRequest, CertThumbprint);
                respString = Convert.ToBase64String(resp);
                resp = Encoding.ASCII.GetBytes(respString);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            // Шифрование
            try
            {
                resp = Cryptography.CryptoPro.EncryptMsg(resp, new X509Certificate2Collection() { CertThumbprint });
                //respString = Encoding.GetEncoding("windows-1251").GetString(resp, 0, resp.Length);
                //respString = Convert.ToBase64String(resp);
                //resp = Encoding.GetEncoding("windows-1251").GetBytes(respString);
                respString = Convert.ToBase64String(resp);
                resp = Encoding.ASCII.GetBytes(respString);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            File.WriteAllBytes(fileResultPath, resp);
        }

        /// <summary>
        /// Метод шифрования и подписи для Эквифакс
        /// </summary>
        /// <param name="clearRequest"></param>
        /// <param name="fileResultPath"></param>
        /// <param name="CertThumbprint"></param>
        public static void Equifax(byte[] clearRequest, string fileResultPath, X509Certificate2 CertThumbprint)
        {
            // Подпись
            try
            {
                resp = Cryptography.CryptoPro.SignMsg(clearRequest, CertThumbprint);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            // Шифрование
            try
            {
                resp = Cryptography.CryptoPro.EncryptMsg(resp, new X509Certificate2Collection() { CertThumbprint });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            File.WriteAllBytes(fileResultPath, resp);
        }
    }
}
