﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.IO;

namespace SigEncEmul
{
    class Program
    {
        static void Main(string[] args)
        {
            string helpMessage = "Argument 1: enc/dec full file path with full file name.\n" +
                    "Argument 2: result full file path with full file name.\n" +
                    "Argument 3: action \"enc\" or \"dec\"\n" +
                    "Argument 4: BCH name in \"NBCH\", \"Experian\", \"Equifax\".\n" +
                    "Argument 5: thumbprint -- thumbprint sertificate for sign. Format: 87C15E3ECC008E616113C0D18C4E8518F10CF076\n";

            if (args.Length < 5)
            {
                Console.WriteLine(helpMessage);
                Console.ReadLine();
            }
            else
            {
                string filePath = args[0];
                string fileResultPath = args[1];
                string action = args[2];
                string bchName = args[3];
                string thumbprint = args[4];

                byte[] clearRequest = new byte[0];
                X509Certificate2 CertThumbprint = new X509Certificate2();

                try
                {
                    CertThumbprint = Cryptography.CryptoPro.GetSignerCert(thumbprint);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }

                using (FileStream byteFileStream = File.OpenRead(filePath))
                {
                    clearRequest = new byte[byteFileStream.Length];
                    byteFileStream.Read(clearRequest, 0, clearRequest.Length);
                }

                if (action == "enc")
                {
                    if (bchName == "NBCH")
                    {
                        Encrypt.Nbch(clearRequest, fileResultPath, CertThumbprint);
                    }
                    else if (bchName == "Experian")
                    {
                        Encrypt.Experian(clearRequest, fileResultPath, CertThumbprint);
                    }
                    else if (bchName == "Equifax")
                    {
                        Encrypt.Equifax(clearRequest, fileResultPath, CertThumbprint);
                    }
                    else
                    {
                        Console.ReadLine();
                    }
                }
                else if (action == "dec")
                {
                    if (bchName == "NBCH")
                    {
                        Decrypt.Nbch(clearRequest, fileResultPath, CertThumbprint);
                    }
                    else if (bchName == "Experian")
                    {
                        Decrypt.Experian(clearRequest, fileResultPath, CertThumbprint);
                    }
                    else if (bchName == "Equifax")
                    {
                        Decrypt.Equifax(clearRequest, fileResultPath, CertThumbprint);
                    }
                    else
                    {
                        Console.ReadLine();
                    }
                }
                else
                {
                    Console.ReadLine();
                }
            }
            
        }
    }
}
