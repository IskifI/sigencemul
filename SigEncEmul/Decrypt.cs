﻿using System;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.IO;

namespace SigEncEmul
{
    /// <summary>
    /// Класс находится в разработке, работает ТОЛЬКО дешифровка и ТОЛЬКО по НБКИ/Эквифакс
    /// По ОКБ для дешифровки вероятно необходимо сначала дешифровывать base64
    /// </summary>
    public class Decrypt
    {
        static byte[] resp = new byte[0];

        /// <summary>
        /// Метод дефишфровки и снятия подписи с НБКИ
        /// </summary>
        /// <param name="clearRequest"></param>
        /// <param name="fileResultPath"></param>
        /// <param name="CertThumbprint"></param>
        public static void Nbch (byte[] clearRequest, string fileResultPath, X509Certificate2 CertThumbprint)
        {
            // Расшифрование
            try
            {
                resp = Cryptography.CryptoPro.DecryptMsg(clearRequest);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            File.WriteAllBytes(fileResultPath, resp);
        }

        /// <summary>
        /// Метод дешифровки и снятия подписи с ОКБ
        /// </summary>
        /// <param name="clearRequest"></param>
        /// <param name="fileResultPath"></param>
        /// <param name="CertThumbprint"></param>
        public static void Experian(byte[] clearRequest, string fileResultPath, X509Certificate2 CertThumbprint)
        {

            // Расшифрование
            try
            {
                resp = Cryptography.CryptoPro.DecryptMsg(clearRequest);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            File.WriteAllBytes(fileResultPath, resp);
        }

        /// <summary>
        /// Метод дешифровки и снятия подписи с Эквифакс
        /// </summary>
        /// <param name="clearRequest"></param>
        /// <param name="fileResultPath"></param>
        /// <param name="CertThumbprint"></param>
        public static void Equifax(byte[] clearRequest, string fileResultPath, X509Certificate2 CertThumbprint)
        {
            // Расшифрование
            try
            {
                resp = Cryptography.CryptoPro.DecryptMsg(clearRequest);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            File.WriteAllBytes(fileResultPath, resp);
        }
    }
}
